# asp_projekt



## O projektu

Program izčitava podatke iz csv. file-a te nad tim podatcima vrši pretragu, dodavanje, brisanje i traženje najmanje i najveće vrijednosti. U ovom projektu sam koristio vektor, on mi se činio kao najbolji izbor. Za pretragu sam koristio binary search, složenost je O(log2n). Za min i max vrijednost sam koristio max_element i min_element iz algorithm-a. Činio mi se kao najednostavniji način za pretragu najveće i najmanje vrijednosti. Cijeli projekt je izrađen u clasi Asp.

## Problemi

Tokom izrade projekta naišao sam na par problema. Prvi problem je bio u koji tip varijable da pohranim brojeve jer su bili preveliki za double, te sam koristio string. Drugi problem je bio kod min i max_element, Pošto sam pretraživao u stringu mogao je najviše ići do 100 i najmanje do 0. Tu samo morao koristiti double i funkciju transform kako bi to radilo. 

## Mjerenje

 Funkcija dodaj() = 4590 ms ? 
 Funkcija obrisi() = 25ms
 Funkcija trazi() = 2525 ms
 Funkcija getTopValue() = 776 ms
 Funkcija getBottomValue() = 732 ms
