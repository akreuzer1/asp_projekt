#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <chrono>


class Asp{
private:
    std::vector<double>rezultat;
    std::string line,u_q,coolant,stator_winding;
public:
    std::vector<std::string> stupac1;
    std::vector<std::string> stupac2;
    std::vector<std::string> stupac3;


    //Pohranjujemo stupce u vektore

    void pohrani_u_vektor() {
        std::ifstream file("E://FAKS//4.semsestar//asp//measures_v2.csv");
        while (std::getline(file, line)) {
            std::stringstream linestream(line);
            std::getline(linestream, u_q, ',');
            std::getline(linestream, coolant, ',');
            std::getline(linestream, stator_winding, ',');
            stupac1.push_back(u_q);
            stupac2.push_back(coolant);
            stupac3.push_back(stator_winding);
        }
    }

    void getTopValue(std::vector<std::string>& v){
        std::transform(v.begin()+1, v.end(), std::back_inserter(rezultat),[](const std::string& str) { return std::stod(str); });

        std::cout<< std::setprecision(12) << *max_element(rezultat.begin(), rezultat.end());

    }

    void getBottomValue(std::vector<std::string>& v){
        std::transform(v.begin()+1, v.end(), std::back_inserter(rezultat),[](const std::string& str) { return std::stod(str); });

        std::cout<< std::setprecision(12) << *min_element(rezultat.begin(), rezultat.end());

    }




    void dodaj(std::vector<std::string>& v,std::vector<std::string>& key){
        for(int i =0;i<key.size();i++) {
            v.push_back(key[i]);
        }
        std::cout <<"Dodao sam";
    }


    void obrisi(std::vector<std::string>& v,std::vector<std::string>& key){

        for (int i=0;i<key.size();i++){
            auto iterator = std::remove(v.begin(), v.end(), key[i]);
            v.erase(iterator, v.end());
            }
        std::cout <<"Obrisao sam";


    }

    void trazi(std::vector<std::string>& v,std::vector<std::string>& key){
        std::sort(v.begin(),v.end(), std::less<>());

        for(auto i : key){
            if(std::binary_search(v.begin(), v.end(), i)){
                std::cout << std::setprecision(12) <<"Pronasao sam broj: "<<i<<std::endl;
            }
            else{
                std::cout << std::setprecision(12) << "Nisam uspio naci broj: "<<i <<std::endl;
            }
        }


    }




};

using namespace std;
int main()
{
    Asp asp;
    std::vector<std::string> pretraga = {"6","4"};
    std::vector<std::string> unesi = {"6","4"};
    std::vector<std::string> brisi = {"6","4"};
    asp.pohrani_u_vektor();

    auto start = std::chrono::high_resolution_clock::now();
    //asp.dodaj(asp.stupac1,unesi);
    //asp.obrisi(asp.stupac1,brisi);
    //asp.getTopValue(asp.stupac1);
    //asp.getBottomValue(asp.stupac1);

    asp.trazi(asp.stupac1, pretraga);
    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout<<"Vrijeme je: "<<duration.count()<<"ms"<<std::endl;

}
